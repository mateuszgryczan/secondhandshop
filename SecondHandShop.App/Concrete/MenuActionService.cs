﻿using SecondHandShop.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHandShop.App.Concrete
{
    public class MenuActionService
    {
        List<MenuAction> MenuActions;

        public MenuActionService()
        {
            MenuActions = new List<MenuAction>();
            Initialize();
        }

        public void ShowActionsByMenuName(string menuName)
        {
            List<MenuAction> menuActions = GetMenuActionsByMenuName(menuName);

            foreach (MenuAction menuAction in menuActions)
            {
                Console.WriteLine($"{menuAction.Id}. {menuAction.Context}");
            }
        }


        public List<MenuAction> GetMenuActionsByMenuName(string menuName)
        {
            List<MenuAction> actions = new List<MenuAction>();

            foreach (MenuAction menuAction in MenuActions)
            {
                if (menuAction.MenuName == menuName)
                {
                    actions.Add(menuAction);
                }
            }

            return actions;
        }

        private void Initialize()
        {
            MenuActions.Add(new MenuAction(1, "Add", "mainMenu"));
            MenuActions.Add(new MenuAction(2, "Delete", "mainMenu"));
            MenuActions.Add(new MenuAction(3, "Update", "mainMenu"));
            MenuActions.Add(new MenuAction(4, "Show all items", "mainMenu"));

            MenuActions.Add(new MenuAction(1, "Update the name", "updateMenu"));
            MenuActions.Add(new MenuAction(2, "Update the quantity", "updateMenu"));
        }
    }
}
