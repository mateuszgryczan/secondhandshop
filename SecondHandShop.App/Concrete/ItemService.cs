﻿using SecondHandShop.App.Common;
using SecondHandShop.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHandShop.App.Concrete
{
    public class ItemService : BaseService<Item>
    {
        public void UpdateItemNameById(int itemId, string itemNewName)
        {
            foreach (Item item in Items)
            {
                if (item.Id == itemId)
                {
                    item.Name = itemNewName;
                }
            }
        }

        public void UpdateItemQuantityById(int itemId, int itemQuantity)
        {
            foreach (Item item in Items)
            {
                if (item.Id == itemId)
                {
                    item.Quantity = itemQuantity;
                }
            }
        }
    }
}
