﻿using SecondHandShop.App.Abstract;
using SecondHandShop.App.Concrete;
using SecondHandShop.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHandShop.App.Managers
{
    //Ta klasa będzie controlerem, który będzie decydował co użytkownik chcę zrobić.
    //Na podstawie tego będzie wykonywał akcje i będzie wywoływał odpowiednie metody z serwisu.
    public class ItemManager
    {
        private readonly MenuActionService _menuActionService;
        private ItemService _itemService;

        public ItemManager(MenuActionService menuActionService, ItemService itemService)
        {
            _menuActionService = menuActionService;
            _itemService = itemService;
        }

        public int AddItemView()
        {
            List<MenuAction> menuActions = _menuActionService.GetMenuActionsByMenuName("itemMenu");

            int itemLastId = _itemService.GetLastId();

            Console.Write("\nName: ");
            string name = Console.ReadLine();

            Console.Write("Quantity: ");
            string strQuantity = Console.ReadLine();
            int quantity;
            Int32.TryParse(strQuantity, out quantity);
            Console.Write("\n");

            Item item = new Item(itemLastId + 1, name, quantity);

            _itemService.AddItem(item);

            return item.Id;
        }

        public int RemoveItemById()
        {
            Console.Write("\nThe id of item you want to remove: ");
            string outputId = Console.ReadLine();
            Console.Write("\n");
            int itemId;
            Int32.TryParse(outputId, out itemId);

            Item item = _itemService.GetItemById(itemId);

            _itemService.RemoveItem(item);

            return itemId;
        }
        public void UpdateItemName()
        {
            Console.Write("Enter the id of item to update.\n");
            Console.Write("Id: ");
            string itemIdStr = Console.ReadLine();
            Console.Write("\n");
            int itemId;
            Int32.TryParse(itemIdStr, out itemId);

            Console.Write("Enter the new item name.\n");
            Console.Write("Name: ");
            string itemName = Console.ReadLine();
            Console.Write("\n");

            _itemService.UpdateItemNameById(itemId, itemName);
        }

        public void UpdateItemQuantity()
        {
            Console.Write("Enter the id of item to update.\n");
            Console.Write("Id: ");
            string itemIdStr = Console.ReadLine();
            Console.Write("\n");
            int itemId;
            Int32.TryParse(itemIdStr, out itemId);

            Console.Write("Enter the new item quantity.\n");
            Console.Write("Quantity: ");
            string itemQuantityStr = Console.ReadLine();
            Console.Write("\n");
            int itemQuantity;
            Int32.TryParse(itemQuantityStr, out itemQuantity);

            _itemService.UpdateItemQuantityById(itemId, itemQuantity);
        }

        public void ShowAllItems()
        {
            List<Item> items = _itemService.GetAllItems();

            if (items.Count > 0)
            {
                foreach (Item item in items)
                {
                    Console.Write($"\n{item.Id}. {item.Name} - {item.Quantity}\n\n");
                }
            }
            else
            {
                Console.Write("\nSorry, the list is empty.\n\n");
            }
        }
    }
}
