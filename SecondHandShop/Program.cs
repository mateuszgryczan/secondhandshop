﻿using SecondHandShop.App.Abstract;
using SecondHandShop.App.Concrete;
using SecondHandShop.App.Managers;
using SecondHandShop.Domain.Entity;
using System;

namespace SecondHandShop
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MenuActionService menuActionService = new MenuActionService();
            ItemService itemService = new ItemService();
            ItemManager itemManager = new ItemManager(menuActionService, itemService);

            Console.WriteLine("Welcome to the SecondHandShop!");
            while (true)
            {
                menuActionService.ShowActionsByMenuName("mainMenu");
                Console.Write("\nOption: ");
                ConsoleKeyInfo optionMainMenu = Console.ReadKey();
                Console.Write("\n");

                switch (optionMainMenu.KeyChar)
                {
                    case '1':
                        int addedItemId = itemManager.AddItemView();
                        break;
                    case '2':
                        int removedItemId = itemManager.RemoveItemById();
                        break;
                    case '3':
                        Console.Write("\n");
                        menuActionService.ShowActionsByMenuName("updateMenu");
                        Console.Write("\nOption: ");
                        ConsoleKeyInfo optionUpdateMenu = Console.ReadKey();
                        Console.Write("\n");

                        switch (optionUpdateMenu.KeyChar)
                        {
                            case '1':
                                itemManager.ShowAllItems();
                                itemManager.UpdateItemName();
                                break;
                            case '2':
                                itemManager.ShowAllItems();
                                itemManager.UpdateItemQuantity();
                                break;
                            default:
                                Console.Write("\nThe action you've entered doesn't exist.\n\n");
                                break;
                        }
                        break;
                    case '4':
                        itemManager.ShowAllItems();
                        break;
                    default:
                        Console.Write("\nThe action you've entered doesn't exist.\n\n");
                        break;
                }
            }

        }
    }
}
