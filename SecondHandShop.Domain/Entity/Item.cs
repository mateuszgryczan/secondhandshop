﻿using SecondHandShop.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHandShop.Domain.Entity
{
    public class Item : BaseEntity
    {
        public string Name { get; set; }
        public int Quantity { get; set; }

        public Item(int id, string name, int quantity)
        {
            Id = id;
            Name = name;
            Quantity = quantity;
        }
    }
}
