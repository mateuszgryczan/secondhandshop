﻿using SecondHandShop.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace SecondHandShop.Domain.Entity
{
    public class MenuAction : BaseEntity
    {
        public string Context { get; set; }
        public string MenuName { get; set; }

        public MenuAction(int id, string context, string menuName)
        {
            Id = id;
            Context = context;
            MenuName = menuName;
        }
    }
}
